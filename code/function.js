// - Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

function filterBy(arr, type) {
   let newArr = [];
   for (let i = 0; i<arr.length; i++) {
      if (type == 'number' && typeof arr[i] !== type) {
         if (arr[i] === null) {
            continue;
         }
         newArr.push(arr[i]);
      } else if (typeof arr[i] !== type) {         
         newArr.push(arr[i]);
      }
   }
   return newArr;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));